package com.dlse.taller.auth.service.service;

import com.dlse.taller.auth.service.model.JwtAttributes;
import com.dlse.taller.auth.service.util.JwtType;

import io.jsonwebtoken.JwtBuilder;

public interface JwtService {
	
	public JwtBuilder getSignedJwtWithKeys(JwtAttributes attributes) throws Exception;
	
	public JwtAttributes configureJwt(JwtType kind);
	
	public Boolean isJwtValid(String token) throws Exception;

}
