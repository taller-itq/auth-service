package com.dlse.taller.auth.service.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class Constants {
	
	public static final String TOKEN_TTL = "300";
	public static final String TOKEN_NAME = "session-tkn";
	public static final String TOKEN_SECRET = "QmVhdXR5TWUtRExTRS10b2tlblNlY3JldF81ZU5jMGQzRGVU";
	public static final String ERROR_MSG = "Error occurred in jwt-svc API";
	
	@Value("${dlse.jwt.private-key-location}")
	public static String TOKEN_PATH;
	
	@Value("${dlse.jwt.public-key-location}")
	public static String PUB_TOKEN_PATH;
	
	@Autowired
	Constants(Environment env){
		this.TOKEN_PATH = env.getProperty("dlse.jwt.private-key-location"); 
		this.PUB_TOKEN_PATH = env.getProperty("dlse.jwt.public-key-location");
	}

}