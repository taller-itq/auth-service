package com.dlse.taller.auth.service.model.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class JwtRequest {
	
	@NotBlank
	String svcName;
	@NotBlank
	String token;

}