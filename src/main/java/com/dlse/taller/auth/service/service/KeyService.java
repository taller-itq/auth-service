package com.dlse.taller.auth.service.service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

public interface KeyService {
	
	public PrivateKey convertToPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException;
	public PublicKey convertToPublic() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException;

}