package com.dlse.taller.auth.service.exception;

public class JwtException extends RuntimeException {
	
	public JwtException(String msg) {
		super(msg);
	}
	
	public JwtException(String msg, Throwable twb) {
		super(msg, twb);
	}

}