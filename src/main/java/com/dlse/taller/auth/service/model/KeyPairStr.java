package com.dlse.taller.auth.service.model;

import lombok.Data;

@Data
public class KeyPairStr {
	
	private String privateKey;
	private String publicteKey;

}
