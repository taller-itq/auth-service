package com.dlse.taller.auth.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import com.dlse.taller.auth.service.model.JwtAttributes;
import com.dlse.taller.auth.service.model.request.JwtRequest;
import com.dlse.taller.auth.service.model.response.BaseResponse;
import com.dlse.taller.auth.service.model.response.ExceptionResponse;
import com.dlse.taller.auth.service.model.response.JwtResponse;
import com.dlse.taller.auth.service.service.JwtService;
import com.dlse.taller.auth.service.util.JwtType;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@Validated
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class JwtController {

	private final JwtService jwtService;

	@GetMapping("/issue")
	public ResponseEntity<? extends BaseResponse> getToken() {
		log.info("in jwt service");
		log.info("svc: {}", jwtService);
		try {
			JwtAttributes jwt = jwtService.configureJwt(JwtType.valueOf("SESSION"));
			JwtBuilder jws = jwtService.getSignedJwtWithKeys(jwt);
			return ResponseEntity.status(HttpStatus.CREATED)
					.body(JwtResponse.builder()
							.message("token issued.")
							.claim(jws.compact())
							.isValid(true)
							.build());

		} catch (HttpServerErrorException e) {
			return ResponseEntity.status(e.getStatusCode())
					.body(ExceptionResponse.builder()
							.message(e.getMessage())
							.execptionType(e.getClass().getName())
							.build());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(ExceptionResponse.builder()
							.message(e.getMessage())
							.execptionType(e.getClass().getName())
							.build());
		} finally {
		}
	}

	@PostMapping("/validate")
	public ResponseEntity<? extends BaseResponse> validateToken(@RequestBody @Valid JwtRequest requestBody) {
		log.info("in jwt service");
		try {
			Boolean isValid = jwtService.isJwtValid(requestBody.getToken());
			log.info("isValid? {}", isValid);
			return ResponseEntity.status(HttpStatus.OK)
					.body(JwtResponse.builder()
							.message("token validated.")
							.claim(requestBody.getToken())
							.isValid(isValid)
							.build());
		} catch (ExpiredJwtException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
					.body(ExceptionResponse.builder()
							.message(e.getMessage())
							.execptionType(e.getClass().getName())
							.build());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(ExceptionResponse.builder()
							.message(e.getMessage())
							.execptionType(e.getClass().getName())
							.build());
		} finally {
		}
	}

}
