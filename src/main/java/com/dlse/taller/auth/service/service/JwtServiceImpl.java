package com.dlse.taller.auth.service.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dlse.taller.auth.service.exception.JwtException;
import com.dlse.taller.auth.service.model.JwtAttributes;
import com.dlse.taller.auth.service.util.Constants;
import com.dlse.taller.auth.service.util.JwtType;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.SignatureException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.dlse.taller.auth.service.util.Constants.ERROR_MSG;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class JwtServiceImpl implements JwtService{
	
	private final KeyService keyService;

	@Override
	public JwtBuilder getSignedJwtWithKeys(JwtAttributes attributes) throws Exception {
		try {
			Date now = new Date(System.currentTimeMillis());
			JwtBuilder jws;
			jws = Jwts.builder()
					.setHeaderParam("knd", "session")
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(attributes.getExp())
					.claim("tknName", attributes.getTknName())
					.claim("tknKnd", attributes.getKnd())
					.setNotBefore(new Date(System.currentTimeMillis()))
					.setId(attributes.getUuid());
			return jws.signWith(keyService.convertToPrivateKey(), SignatureAlgorithm.RS512);
		} catch (Exception e) {
			log.error("{}, {}",e.getClass().getName(), e.getMessage());
			throw new JwtException(ERROR_MSG+" in JWT Service: "+e.getMessage(), e.getCause());
		} finally {
		}
	}

	@Override
	public JwtAttributes configureJwt(JwtType kind) {
		try {
			JwtAttributes attributes = new JwtAttributes();
			
			attributes.setExp(new Date(System.currentTimeMillis() + Integer.parseInt(Constants.TOKEN_TTL) * 1000));
			attributes.setKnd(kind.toString());
			attributes.setTknName(Constants.TOKEN_NAME+"_"+UUID.randomUUID().toString());
		
			return attributes;
		} catch (Exception e) {
			log.error("{}, {}",e.getClass().getName(), e.getMessage());
			throw new JwtException(ERROR_MSG+" in JWT Service: "+e.getMessage(), e.getCause());
		} finally {
		}
	}

	@Override
	public Boolean isJwtValid(String token) throws NoSuchAlgorithmException, InvalidKeySpecException {
		try {
			JwtParser builder = Jwts.parserBuilder().setSigningKey(keyService.convertToPrivateKey()).build();
			Claims claims = builder.parseClaimsJws(token).getBody();
			
			claims.forEach((k,v) -> log.debug("k: {}, v: {}", k, v));
			return !claims.getExpiration().before(new Date());
		} catch(SignatureException e) {
			log.error("{}, {}",e.getClass().getName(), e.getMessage());
			throw e;
		} catch(ExpiredJwtException e) {
			log.error("{}, {}",e.getClass().getName(), e.getMessage());
			throw e;
		} catch(UnsupportedJwtException e) {
			log.error("{}, {}",e.getClass().getName(), e.getMessage());
			throw e;
		} catch(MalformedJwtException e) {
			log.error("{}, {}",e.getClass().getName(), e.getMessage());
			throw e;
		} catch(NoSuchAlgorithmException e) {
			log.error("{}, {}",e.getClass().getName(), e.getMessage());
			throw e;
		}  catch(InvalidKeySpecException e) {
			log.error("{}, {}",e.getClass().getName(), e.getMessage());
			throw e;
		} catch (Exception e) {
			log.error("{}, {}",e.getClass().getName(), e.getMessage());
			throw new JwtException(ERROR_MSG+" in JWT Service: "+e.getMessage(), e.getCause());
		} finally {
		}
	}

}
