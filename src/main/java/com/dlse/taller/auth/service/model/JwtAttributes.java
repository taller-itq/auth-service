package com.dlse.taller.auth.service.model;

import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;

@Data
public class JwtAttributes {
	
	private Date issued;
	private Date exp;
	private String knd;
	private String tknName;
	private String uuid;
	
	public JwtAttributes() {
		this.uuid = UUID.randomUUID().toString();
	}
	
	public boolean areAttributesSet() {
		return StringUtils.isNotBlank(knd)
				&& StringUtils.isNotBlank(tknName);
	}

}
