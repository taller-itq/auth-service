package com.dlse.taller.auth.service.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dlse.taller.auth.service.util.Constants;
import com.dlse.taller.auth.service.util.Util;
import com.google.common.annotations.VisibleForTesting;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class KeyServiceImpl implements KeyService{
	
	private final Util util;

	private KeyFactory factory;
	
	public PrivateKey convertToPrivateKey() 
			throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		try {
			File f = new File(Constants.TOKEN_PATH);
			byte[] privateKey = Files.readAllBytes(f.toPath());
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKey);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			return keyFactory.generatePrivate(keySpec);
		} finally {
		}
	}
	
	public PublicKey convertToPublic()
			throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		try {
			File f = new File(Constants.PUB_TOKEN_PATH);
			byte[] privateKey = Files.readAllBytes(f.toPath());
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(privateKey);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			return keyFactory.generatePublic(keySpec);
		} finally {
		}
	}

}