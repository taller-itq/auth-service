package com.dlse.taller.auth.service.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse {
	
	private String message;

}
