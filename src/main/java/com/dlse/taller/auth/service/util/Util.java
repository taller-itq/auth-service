package com.dlse.taller.auth.service.util;

import javax.xml.bind.DatatypeConverter;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Util {
	
	public byte[] decode(final String str) {
		String pre = str.replace('-','+');
		pre = pre.replace('_', '=');
		return DatatypeConverter.parseBase64Binary(pre);
	}

}
